#!/bin/bash
ROOT_UID=0
SUCCESS=0

ARCH_USERS_NO_AGREGADOS="userNOAgregados"
ARCH_DIR_AGREGADOS="dirAgregados"
ARCH_TEMP="_temp"
ARCH_USER_AGREGADOS="userAgregados"
ARCH_AUX="arch_Aux"

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
# $UID ---> User ID (UID) 0 -> root
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe indicar el archivo con el listado de usuarios a ingresar..."
   exit 1
fi

# Del archivo con el listado de usuarios a ingresar:
# Este es el formato (Ejemplo 1):
# ejemplo:9010_fher:575:0:BO-P.ABIERTAS FERNANDO MERCADO:/home/ejemplo:/bin/bash: 
#    |       |       |  |                       |              |          |
#    f1      f2      f3 f4                      f5             f6         f7 

# Este es el formato (Ejemplo 2):
# ejemplo:9010_fher:575:0:BO-P.ABIERTAS FERNANDO MERCADO::/bin/bash: 
#    |       |       |  |                       |       |       |
#    f1      f2      f3 f4                      f5      f6      f7 

# Este es el formato (Ejemplo 3):
# ejemplo:9010_fher:575::BO-P.ABIERTAS FERNANDO MERCADO::/bin/bash: 
#    |       |      |  |                       |       |       |
#    f1      f2    f3  f4                      f5      f6      f7 

#$f1 = username
#$f2 = password *por defecto es el nombre de usuario, pero se debe cambiar con el primer inicio de sesión
#$f3 = User ID *no se utiliza sí se deja en blanco
#$f4 = Group ID (GID), este ID debe existir en /etc/group
#$f5 = User ID INFO
#$f6 = home directory *no se utiliza sí se deja en blanco
#$f7 = comand shell 

>$ARCH_AUX
>$ARCH_USERS_NO_AGREGADOS
>$ARCH_DIR_AGREGADOS
echo "/home:" >> $ARCH_DIR_AGREGADOS # se da por hecho que home existe
>$ARCH_USER_AGREGADOS

subdirectoriosHome(){
	ls /home/ >> $ARCH_AUX # se agregan todos los subdirectorios de /home/
	while read line; 
	do
		echo -e /home/"$line": >> $ARCH_DIR_AGREGADOS;

	done < $ARCH_AUX
	echo "----------------------------"
	echo "Directorios en /home"
	echo "----------------------------"
	cat $ARCH_DIR_AGREGADOS;
	echo "----------------------------"
}

subdirectoriosHome

crearUsuario(){
	echo "----> Crear Usuario <----"
	eval user="$1"
	eval grupoID="$2"
	eval userInfo="$3"           
	eval stringHomeDir="$4"
	eval pass="$5"
	echo "Opcion shell       = ${6}"
	echo "-------------------------"
	if [ "${stringHomeDir}" = "" ];
	then
		echo " ---> Home Directory en blanco <---"
	else
		caraterBuscar="/"
		numeroVeces=$(grep -o "$caraterBuscar" <<< "${stringHomeDir}" | wc -l)
		echo "Numero de veces de << $caraterBuscar >> en <<$stringHomeDir>> : $numeroVeces"
		directorioHome=$(echo ${stringHomeDir} | cut -d '/' -f -$numeroVeces)
		echo "directorioHome = $directorioHome"
		comparar="$directorioHome":

		grep -q -w "$comparar" $ARCH_DIR_AGREGADOS
		if [ $? -ne $SUCCESS ];
		then
			echo "NO esta, se CREARA el Directorio [$directorioHome]..."
			mkdir --verbose -p "$directorioHome"
			if [ $? -eq $SUCCESS ]; 
			then
				echo "Directorio [$directorioHome] creado correctamente..."
				echo "$comparar" >> $ARCH_DIR_AGREGADOS
				chmod 777 "$directorioHome"
				if [ $? -eq $SUCCESS ];
				then
					echo "Permisos cambiados correctamente al directorio << $directorioHome >>"
				else
					echo "ERROR al cambiar Permisos al directorio << $directorioHome >>"
				fi
			else
				echo "Directorio [$directorioHome] NO se creo correctamente..."
			fi
		else
			echo "Esta, NO Se CREARA el Directorio [$directorioHome]..."
		fi
	fi

	if [ ${6} == "1" ];	
	then
		if [ "${stringHomeDir}" = "" ];	# Si home directory esta en blanco
		then
			echo "Home Directory en blanco... Opcion 1"
			if [ "${grupoID}" = "" ]; # Si grupo ID esta en blanco
			then
				echo "Grupo ID en blanco... Opcion 1"
				useradd -p "${pass}" -M -N -s /bin/bash -c "${userInfo}" "${user}"
				if [ $? -eq 0 ];
				then
					echo " ---->Usuario [ ${user} ] ha sido agregado al sistema!!!"
					chage -d 0 "${user}" # Cambiar el password al primer login
					echo "${user}" >> $ARCH_USER_AGREGADOS
				else
					echo " ---> Ocurrio un fallo al agregar el usuario [ ${user} ]!!!"
					echo "Error en useradd ---> No se pudo agregar el usuario [ ${user} ]" >> $ARCH_USERS_NO_AGREGADOS
				fi
				echo " "
			else 	# Si grupo ID esta NO en blanco
				echo "Grupo ID NO en blanco... Opcion 1"
				useradd -p "${pass}" -M -g "${grupoID}" -s /bin/bash -c "${userInfo}" "${user}"
				if [ $? -eq 0 ];
				then
					echo " ---->Usuario [ ${user} ] ha sido agregado al sistema!!!"
					chage -d 0 "${user}" # Cambiar el password al primer login
					echo "${user}" >> $ARCH_USER_AGREGADOS
				else
					echo " ---> Ocurrio un fallo al agregar el usuario [ ${user} ]!!!"
					echo "Error en useradd ---> No se pudo agregar el usuario [ ${user} ]" >> $ARCH_USERS_NO_AGREGADOS
				fi
				echo " "
			fi
		else						# Si Tiene home directory
			echo "Home Directory NO en blanco... Opcion 1"
			if [ "${grupoID}" = "" ]; # Si grupo ID esta en blanco
			then
				echo "Grupo ID en blanco... Opcion 1"
				useradd -p "${pass}" -d "$directorioHome"/"${user}" -m -N -s /bin/bash -c "${userInfo}" "${user}"
				if [ $? -eq 0 ];
				then
					echo " ---->Usuario [ ${user} ] ha sido agregado al sistema!!!"
					chage -d 0 "${user}"	# Cambiar el password al primer login
					echo "${user}" >> $ARCH_USER_AGREGADOS
				else
					echo " ---> Ocurrio un fallo al agregar el usuario [ ${user} ]!!!"
					echo "Error en useradd ---> No se pudo agregar el usuario [ ${user} ]" >> $ARCH_USERS_NO_AGREGADOS
				fi
				echo " "
			else  # Si grupo ID esta NO en blanco	
				echo "Grupo ID NO en blanco... Opcion 1"			
				useradd -p "${pass}" -d "$directorioHome"/"${user}" -m -g "${grupoID}" -s /bin/bash -c "${userInfo}" "${user}"
				if [ $? -eq 0 ];
				then
					echo " ---->Usuario [ ${user} ] ha sido agregado al sistema!!!"
					chage -d 0 "${user}"	# Cambiar el password al primer login
					echo "${user}" >> $ARCH_USER_AGREGADOS
				else
					echo " ---> Ocurrio un fallo al agregar el usuario [ ${user} ]!!!"
					echo "Error en useradd ---> No se pudo agregar el usuario [ ${user} ]" >> $ARCH_USERS_NO_AGREGADOS
				fi
				echo " "
			fi
		fi	
	else
		echo "Opcion ${6} desconocida..."
	fi
}

while IFS=: read -r f1 f2 f3 f4 f5 f6 f7
do
	nombreUsuario=$(echo $f1 | sed 's/ //g') # Elimina los espacios en blanco
	passwordX=$(echo $f2 | sed 's/ //g')
	userID=$(echo $f3 | sed 's/ //g')
	groupID=$(echo $f4 | sed 's/ //g')
	user_ID_INFO=$(echo $f5 | sed 's/^ //g' | sed 's/ $//g') # Elimina los blancos al principio y al extremo
	homeDirectory=$(echo $f6 | sed 's/ //g')
	comandShell=$(echo $f7 | sed 's/ //g')
	
	echo "El shell es: $comandShell"

	# Revisa si el usuario existe.
	# -eq = es igual
	grep -q -w "$nombreUsuario" /etc/passwd 
	if [ $? -eq $SUCCESS ]; 
	then	
		echo " ---> Usuario [ $nombreUsuario ] ya existe en /etc/passwd ---> NO AGREGADO"
		echo "Usuario [ $nombreUsuario ] NO agregado  ---> En /etc/passwd ya existe el usuario [ $nombreUsuario ]" >> $ARCH_USERS_NO_AGREGADOS
		echo " "
	else
		echo "Usuario [ $nombreUsuario ] NO Existe..."
		# Verifica que el ID del grupo exista
		# -ne = no es igual
		grep -q -w "$groupID" /etc/group
		if [ $? -ne $SUCCESS ]; 
		then	
			echo " ---> Grupo [ $groupID ] NO existe en /etc/group ---> USUARIO [$nombreUsuario] NO AGREGADO"
			echo "Usuario [ $nombreUsuario ] NO agregado  ---> En /etc/group No existe el grupo con ID [ $groupID ]" >> $ARCH_USERS_NO_AGREGADOS
			echo " "
		else
			echo "Grupo [ $groupID ] SI existe..."
			# Estos son los tres shell vienen en la configuración
			shell_1="/bin/bash:"
			shell_2="/usr/bin/passwd:"
			shell_3="/bin/bash/passwd:"
			>$ARCH_TEMP
			echo "$comandShell" >> $ARCH_TEMP
			echo "------------------------------------"
			echo "Salida del archivo <<$ARCH_TEMP>>"
			cat $ARCH_TEMP
			echo "------------------------------------"
			#sed -i 's%/%-%g' $ARCH_TEMP # tambien podria funcionar,... cambia el / por -
			sed -i 's/.$/:/g' $ARCH_TEMP # agrega : al final de la cadena
			echo "------------------------------------"
			echo "Salida del archivo <<$ARCH_TEMP>>"
			cat $ARCH_TEMP
			echo "------------------------------------"
			grep -q -w "$shell_1" $ARCH_TEMP
			if [ $? -eq $SUCCESS ];
			then
				pass=$(perl -e 'print crypt($ARGV[0], "password")' $nombreUsuario)
				echo "Password generado $pass"
				crearUsuario "\${nombreUsuario}" "\${groupID}" "\${user_ID_INFO}" "\${homeDirectory}" "\${pass}" "1"
			else
				grep -q -w "$shell_2" $ARCH_TEMP
				if [ $? -eq $SUCCESS ];
				then	
			    	pass=$(perl -e 'print crypt($ARGV[0], "password")' $nombreUsuario)
					echo "Password generado $pass"
					crearUsuario "\${nombreUsuario}" "\${groupID}" "\${user_ID_INFO}" "\${homeDirectory}" "\${pass}" "2" 
				else
					grep -q -w "$shell_3" $ARCH_TEMP
					if [ $? -eq $SUCCESS ];
					then	
						pass=$(perl -e 'print crypt($ARGV[0], "password")' $nombreUsuario)
						echo "Password generado $pass"
						crearUsuario "\${nombreUsuario}" "\${groupID}" "\${user_ID_INFO}" "\${homeDirectory}" "\${pass}" "3"
					else					
						echo " ---> El shell es desconocido ---> USUARIO [$nombreUsuario] NO AGREGADO"
						echo "Usuario [ $nombreUsuario ] NO agregado  ---> El shell no se encuentra dentro los validos" >> $ARCH_USERS_NO_AGREGADOS
						echo " "
					fi
				fi
			fi
		fi
	fi	
done < ${file}

rm -f $ARCH_TEMP
if [ $? -eq $SUCCESS ];
then
	echo "Archivo ---> [ $ARCH_TEMP ] borrado..."
fi

rm -f $ARCH_AUX
if [ $? -eq $SUCCESS ];
then
	echo "Archivo ---> [ $ARCH_AUX ] borrado..."
fi

exit 0